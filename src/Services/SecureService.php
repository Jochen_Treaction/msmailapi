<?php

namespace App\Services;
use Psr\Log\LoggerInterface;

class SecureService
{
	private $logger;

	public function __construct(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}


	/**
	 * @param string|null $encyptedPassword
	 * @return string|null
	 */
	public function getDecryptedPasswordFromSecureInOne(string $encyptedPassword=null):?string
	{

		$encPw = (null === $encyptedPassword) ? $_ENV['SMTPSRV_CD'] : $encyptedPassword;
		$secureInOneUrl = $_ENV['SECURE_IN_ONE_URL']; // e.g. https://test-secure-in-one.net

		if(empty($encPw)) {
			$this->logger->critical('msmail: no encrypted password set / found. check SMTPSRV_CD', [__METHOD__, __LINE__]);
			return null;
		}

		if( empty($secureInOneUrl)) {
			$this->logger->critical('msmail: no secure-in-one url set / found. check SECURE_IN_ONE_URL', [__METHOD__, __LINE__]);
			return null;
		}


		$curl = curl_init();
		$pwJson = json_encode(['string' => $encPw]);

		curl_setopt_array($curl, array(
			CURLOPT_VERBOSE =>true,
			CURL_VERSION_DEBUG => true,
			CURLOPT_URL => $secureInOneUrl.'/secure/decstring',
			CURL_HTTP_VERSION_2TLS => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $pwJson,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json'
			),
		));


		$response = curl_exec($curl);
		$this->logger->info('CURL $response' , [$response, __METHOD__, __LINE__]);
		$pwArr = json_decode($response, JSON_OBJECT_AS_ARRAY);
		$this->logger->info('CURL $pwArr' , [$pwArr['string'], __METHOD__, __LINE__]);

		return (!empty($pwArr['string'])) ? $pwArr['string'] : null;

	}

}
