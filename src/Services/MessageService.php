<?php


namespace App\Services;


use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use App\Types\PlaceholderTypes;

/**
 * generates message body (content) based on App\Types\EmailContentTypes->[accountDeletionNotification, accountRegistrationNotification, accountSendActivationlink, and so on ]
 * for a defined set of App\Types\MessageParameterTypes in call to  MessageService::getContent
 * Class MessageService
 * @package App\Services\Messages
 */
class MessageService
{

	const ROOTPATH = '/assets/email-templates/';
	private string $selfContentType;
	private string $linkPath;
	private PlaceholderTypes $placeholder;


	/**
	 * message constructor.
	 * @param string $emailContentType any string of App\Types\EmailContentTypes->[accountDeletionNotification, accountRegistrationNotification, accountSendActivationlink, and so on ]
	 */
	public function __construct(string $emailContentType)
	{
		$this->placeholder = new PlaceholderTypes();
		$emailContentTypes = new EmailContentTypes();
		$l = __LINE__;

		$emailContentTypePropertyName = $emailContentTypes->getPropertyName($emailContentType);
		$l = __LINE__;

		if ($emailContentTypePropertyName) {
			$l = __LINE__;
			$this->selfContentType = $emailContentTypes->{$emailContentTypePropertyName};
		} else {
			throw new Exception("undefined email content type {$emailContentType} ({$l})", __LINE__);
			return;
		}

		$this->linkPath = self::ROOTPATH . $this->selfContentType;
	}


    /**
     * @return string
     */
	private function getBaseUrl(): string
	{
		return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
	}


    /**
     * @return mixed
     */
	private function getMioUrl(){
		return $_ENV['MIOURL'];
	}


    /**
     * @return array
     */
	private function getLinkPlaceholders(): array
	{
		// delete unnecessary
		return $this->placeholder->getPlaceholderTypesArray();
	}


    /**
     * @return string
     */
	private function getImgUrl(): string
	{
		// return $this->getBaseUrl() . $this->linkPath . '/img';
        return $_ENV['EMAIL_IMG_URL'];
	}


    /**
     * @return string
     */
	private function getHtmlFilename(): string
	{
		return $this->selfContentType . '.html';
	}


    /**
     * @return string
     */
	private function getLinkPath(): string
	{
		try {
			$path = $_SERVER['DOCUMENT_ROOT'] . $this->linkPath;
		} catch (Exception $e) {
			$path = 'exception not found';
		}
		return (null !== $path) ? $path : 'not found';
	}


	/**
	 * @param MessageParameterTypes $messageParameters set $messageParameters->[property] to null, if you don't want it to be replaced in corresponding template
	 * @return string
	 */
	public function getContentByMessageParameters(MessageParameterTypes $messageParameters): string
	{
		$content = file_get_contents($this->getLinkPath() . '/' . $this->getHtmlFilename());

		foreach ($this->getLinkPlaceholders() as $placeholder) {
			switch ($placeholder) {
				case $this->placeholder->LASTNAME:
					if (!empty($messageParameters->lastName)) {
						$content = str_replace($placeholder, $messageParameters->lastName, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->FIRSTNAME:
					if (!empty($messageParameters->firstName)) {
						$content = str_replace($placeholder, $messageParameters->firstName, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->EMAIL:
					if (!empty($messageParameters->email)) {
						$content = str_replace($placeholder, $messageParameters->email, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->IMGURL: // always add imgurl
					$content = str_replace($placeholder, $this->getImgUrl(), $content);
					break;
				case $this->placeholder->MIOURL: // always add miourl
					$content = str_replace($placeholder, $this->getMioUrl(), $content);
					break;
				case $this->placeholder->ACTIVATIONLINK:
					if (!empty($messageParameters->activationLink)) {
						$content = str_replace($placeholder, $messageParameters->activationLink, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->ACCOUNTNO:
					if (!empty($messageParameters->accountNo)) {
						$content = str_replace($placeholder, $messageParameters->accountNo, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->APIKEY:
					if (!empty($messageParameters->apiKey)) {
						$content = str_replace($placeholder, $messageParameters->apiKey, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				case $this->placeholder->CUSTOMCONTENT:
					if (!empty($messageParameters->customContent)) {
						$content = str_replace($placeholder, $messageParameters->customContent, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				default:
					; // do nothing
					break;
			}
		}
		return $content;
	}

}
