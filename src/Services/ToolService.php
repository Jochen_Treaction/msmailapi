<?php


namespace App\Services;

use App\Datatypes\EmailType;
use App\Datatypes\SmtpType;
use App\Services\SecureService;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use App\Types\MsMailInterfaceTypes;
use Exception;

/**
 * Class ToolService
 * @package App\Services
 */
class ToolService
{

//  -----------------------------------------------------
//  DEMO CONFIGURATION: Todo: configure data in  CIO-Manager and provide this data from API-IN-ONE
	const STATIC_PW = 'Zlr1$3a8QhipyIcc';
//  SMTPSRV_SENDER=cio.treaction@dev-api-in-one.net
//  SMTPSRV_URL=lvps92-51-150-31.dedicated.hosteurope.de
//  SMTPSRV_PORT=25
//  -----------------------------------------------------
	const SENDER_FULLNAME = 'Marketing-In-One';
	const SMPT_SENDER_DEFAULT = 'cio.treaction@dev-api-in-one.net';
	const SMPT_SERVER_DEFAULT = 'lvps92-51-150-31.dedicated.hosteurope.de';
	const SMPT_PORT_DEFAULT = 25;

	protected $logger;
	protected $mailer;
	protected $emailType;
	protected $secureService;


	public function __construct(LoggerInterface $logger, Swift_Mailer $mailer, EmailType $emailType, SecureService $secureService)
	{
		$this->logger = $logger;
		$this->secureService = $secureService;
		$this->mailer = $mailer;
		$this->emailType = $emailType;
	}


	/**
	 * @return array list of array-keys to provide
	 */
	public function getEmailSettings(): array
	{
		return EmailType::getMailingKeyList();
	}


	/**
	 * @param string      $content
	 * @param string      $message
	 * @param string|null $overwriteMailSubject
	 * @param string|null $overwriteMessageBody
	 * @return bool
	 */
	public function sendEmailByBodyContent(
		string $content,
		string &$message,
		string $overwriteSenderFullName = null,
		string $overwriteMailSubject = null,
		string $overwriteMessageBody = null
	): bool {
		if (!empty($content)) {
			$mailContent = $this->unpackContent($content);


			if (null === $mailContent || !is_array($mailContent)) {
				$message = 'failed to unpack post content';
				return false;
			} else {
				$countRecipients = 0;

				// $email = new EmailType($mailContent);
				$this->emailType->init($mailContent);

				if ($overwriteMailSubject !== null) {
					// $this->emailType->setStringSubject($overwriteMailSubject);
					$this->emailType->setStringSubject($overwriteMailSubject);
				}

				if ($overwriteMessageBody !== null) {
					$this->emailType->getStringMessageBody($overwriteMessageBody);
				}

				if ($overwriteSenderFullName !== null) {
					$this->emailType->setStringSenderFullname($overwriteSenderFullName);
				} else {
					$this->emailType->setStringSenderFullname(self::SENDER_FULLNAME);
				}

				if ($this->emailType->isReadyToSend()) {
					$transport = new Swift_SmtpTransport($this->emailType->getSmtpServer(), (int)$this->emailType->getSmtpPort());
					$transport->setUsername($this->emailType->getStringSenderEmailAddress())->setPassword($this->emailType->getSenderPw());

					try {
						$mailer = new Swift_Mailer($transport);
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					}

					try {
//                        $message = (new Swift_Message())->setSubject($this->emailType->getStringSubject())->setFrom(
//                            [$this->emailType->getStringSenderEmailAddress() => $this->emailType->getStringSenderFullname()]
//                        )->setTo($this->emailType->getRecipientEmailList())->setBody($this->emailType->getStringMessageBody());

						$message = (new Swift_Message())->setSubject($this->emailType->getStringSubject())->setFrom([$this->emailType->getStringSenderEmailAddress() => $this->emailType->getStringSenderFullname()])->setTo($this->emailType->getRecipientEmailList())->addPart($this->emailType->getStringMessageBody() . "XXX TEST XXX",
							'text/html');
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					}

					try {
						$countRecipients = $transport->send($message);
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					}

					$this->logger->info("RECEIPIENTS = " . json_encode(['$countRecipients' => $countRecipients]), [__METHOD__, __LINE__]);

					$message = ($countRecipients > 0) ? "send email to {$countRecipients} recipient(s)" : 'email sending failed';
					return (0 == $countRecipients) ? false : true;
				} else {
					$message = 'email not send: missing data.';
					return false;
				}
			}
		} else {
			$message = 'email not send: no content provided';
			return false;
		}
	}


	public function sendMailBySmtp(string $content, string &$returnMessage): bool
	{
		$returnMessage = '';
		if (!empty($content)) {
			$mailContent = $this->unpackContent($content);
			// TODO: remove
			$this->logger->info('unpacked mailContent=' . print_r($mailContent, true), [__METHOD__, __LINE__]);

			if (null === $mailContent || !is_array($mailContent)) {
				$returnMessage .= 'failed to unpack post content e' . __LINE__;
				return false;
			} else {
				$countRecipients = 0;
				$smtpData = $this->getSmtpSettings();
				// $email = new EmailType($mailContent, $this->logger);
				$this->emailType->init($mailContent);


				$this->logger->info('$mailContent=' . print_r($mailContent, true), [__METHOD__, __LINE__]);

				if ($this->emailType->isReadyToSend()) {
					try {
						$transport = new Swift_SmtpTransport($this->emailType->getSmtpServer(), (int)$this->emailType->getSmtpPort());
						$transport->setUsername($this->emailType->getStringSenderEmailAddress())->setPassword($this->emailType->getSenderPw());
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
						$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
					}


					try {
						$mailer = new Swift_Mailer($transport);
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
						$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
					}

					try {
//                        $message = (new Swift_Message())->setSubject($this->emailType->getStringSubject())->setFrom(
//                            [$this->emailType->getStringSenderEmailAddress() => self::SENDER_FULLNAME]
//                        )->setTo($this->emailType->getRecipientEmailList())->setBody($this->emailType->getStringMessageBody());

						/**
						 * uses html with prio
						 */
						$this->logger->info('$this->emailType->getStringMessageBodyHtml()', [$this->emailType->getStringMessageBody(), __FUNCTION__, __LINE__]);
						$returnMessage = new Swift_Message();
						$returnMessage->setContentType('text/html')->setSubject($this->emailType->getStringSubject())->setBody($this->emailType->getStringMessageBody(),
								'text/html')->setFrom([$this->emailType->getStringSenderEmailAddress() => self::SENDER_FULLNAME])->setTo($this->emailType->getRecipientEmailList());
						// ->addPart($this->emailType->getStringMessageBody(), 'text/html');


					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
						$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
					}

					try {
						$countRecipients = $transport->send($returnMessage);
						$this->logger->info("RECIPIENTS: " . json_encode(['$countRecipients' => $countRecipients]), [__METHOD__, __LINE__]);
					} catch (Exception $e) {
						$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
						$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
					}
					$returnMessage .= ($countRecipients > 0) ? "send email to {$countRecipients} recipient(s)" : 'email sending failed: ' . $countRecipients;
					return (0 == $countRecipients) ? false : true;
				} else {
					$returnMessage .= 'email not send: missing data. e' . __LINE__;
					return false;
				}
			}
		} else {
			$returnMessage .= 'email not send: no content provided. e' . __LINE__;
			return false;
		}
	}


	protected function sendMsg($message)
	{
		try {
			$ret = $this->mailer->send($message);
		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [$e->getCode(), $e->getTrace()], [__METHOD__, __LINE__]);
			return 0;
		}
		return $ret;
	}


	/**
	 * @param string $content = base64_encode(json_encode($array))
	 * @return mixed|null
	 * @author jsr
	 */
	public function unpackContent(string $content)
	{
		if (empty($content)) {
			return null;
		}

		try {
			$unpackedContent = json_decode(base64_decode($content), true, 512, JSON_THROW_ON_ERROR);
			$this->logger->info('$unpackedContent = ', [$unpackedContent, __METHOD__, __LINE__]);
		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
			return null;
		}
		return $unpackedContent;
	}


	/**
	 * @return SmtpType
	 */
	public function getSmtpSettings(): SmtpType
	{
		$smtp = new SmtpType();

		$smtp->setSender(empty($_ENV['SMTPSRV_SENDER']) ? self::SMPT_SENDER_DEFAULT : $_ENV['SMTPSRV_SENDER']);
		// $smtp->setSenderPw($_ENV['SMTPSRV_PW']);
		$smtp->setSenderPw($this->secureService->getDecryptedPasswordFromSecureInOne($_ENV['SMTPSRV_CD']));
		$smtp->setSmtpServer(empty($_ENV['SMTPSRV_URL']) ? self::SMPT_SERVER_DEFAULT : $_ENV['SMTPSRV_URL']);
		$smtp->setSmtpPort(empty($_ENV['SMTPSRV_PORT']) ? self::SMPT_PORT_DEFAULT : $_ENV['SMTPSRV_PORT']);
		return $smtp;
	}
}
