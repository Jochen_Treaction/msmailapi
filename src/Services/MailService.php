<?php


namespace App\Services;

use App\Types\MessageParameterTypes;
use Psr\Log\LoggerInterface;
// use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use App\Types\MsMailInterfaceTypes;
use App\Types\EmailContentTypes;
use App\Services\MessageService;
use Exception;

/**
 * NEW along with App\Types\MsMailInterfaceTypes for simpler handling of email sending
 * Class MailService
 * @package App\Services
 */
class MailService
{
	private LoggerInterface $logger;
	private MsMailInterfaceTypes $mailInterfaceTypes;
	private EmailContentTypes $emailContentTypes;


	public function __construct(LoggerInterface $logger, MsMailInterfaceTypes $mailInterfaceTypes, EmailContentTypes $emailContentTypes)
	{
		$this->logger = $logger;
		$this->mailInterfaceTypes = $mailInterfaceTypes;
		$this->emailContentTypes = $emailContentTypes;

	}


	/**
	 * <b>NEW METHOD !!!</b>
	 * @param string                $emailContentType one of App\Types\EmailContentTypes->[accountSendActivationlink, accountRegistrationNotification, ...]
	 * @param MessageParameterTypes $messageParameters $messageParameters->[firstName, lastName, accountNo, activationLink, ...]
	 * @param MsMailInterfaceTypes  $mailInterface $mailInterface->[messageSubject, senderFullname, senderEmailAddress, ...] can be overwritten in this parameter, if not default settings are taken
	 * @param string                $returnMessage    optional, call by reference => to get back a message of processing result
	 * @return bool
	 * @since 2021-03
     * @internal <b style="color:red">CAUTION: the sympfony/swiftmailer version (no longer supported since 11/2021) this installation does not support non-ASCII characters in email addresses (aka RFC 6532 support)
     *         this installation must be moved to "composer require symfony/mailer" and code must be adapted !!!</b>
	 */
	public function sendSwiftMail(string $emailContentType, MessageParameterTypes $messageParameters, MsMailInterfaceTypes $mailInterface, string &$returnMessage=null): bool
	{
        $params = [
            '$emailContentType' => $emailContentType,
            '$messageParameters' => $messageParameters->getMessageParameterTypesArray(),
            '$mailInterface->senderEmailAddress' => $mailInterface->senderEmailAddress
        ];

        $this->logger->info('PARAMS', [$params, __METHOD__, __LINE__]);

		try {
			$messageService = new MessageService($emailContentType);
		} catch (Exception $e) {
			throw new Exception("Exception in ".__METHOD__.", line ".__LINE__.": ".$e->getMessage(), $e->getCode());
			return false;
		}

		// get email content / message body with placeholder replacements for $messageParameters->[firstName, lastName, accountNo, activationLink, ...]
		$this->logger->info('$messageParameters', [$messageParameters, __METHOD__, __LINE__]);
		if($emailContentType !== 'blank') {
            $mailInterface->messageBody = $messageService->getContentByMessageParameters($messageParameters);
        }

		$returnMessage = '';
		if (!empty($mailInterface->recipientEmailList)) {

			if (empty($mailInterface->messageBody)) {
				$returnMessage .= 'no message body e' . __LINE__;
				return false;
			} else {
				$countRecipients = 0;

				$this->logger->info('$mailContent=' . print_r($mailInterface->messageBody, true), [__METHOD__, __LINE__]);

				try {
					if(587 === (int)$mailInterface->smtpPort) {
						$transport = new Swift_SmtpTransport($mailInterface->smtpServer, (int)$mailInterface->smtpPort, 'tls');
					} else {
						$transport = new Swift_SmtpTransport($mailInterface->smtpServer, (int)$mailInterface->smtpPort );
					}

					$transport->setUsername($mailInterface->senderUsername)->setPassword($mailInterface->senderPassword);

                    $this->logger->info('$transport->getUsername', [$transport->getUsername(), __METHOD__,__LINE__]);
                    $this->logger->info('$transport->getPassword', [$transport->getPassword(), __METHOD__,__LINE__]);

				} catch (Exception $e) {
					$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
				}

				try {
					/**
					 * uses html with prio
					 */
					$swiftMessage = new Swift_Message();
                    $this->logger->info('setFrom', ['senderEmailAddress' => $mailInterface->senderEmailAddress, 'senderFullname' => $mailInterface->senderFullname, __METHOD__, __LINE__]);
					$swiftMessage->setContentType('text/html')
						->setSubject($mailInterface->messageSubject)
						->setBody($mailInterface->messageBody,'text/html')
						->setFrom([$mailInterface->senderEmailAddress => $mailInterface->senderFullname])
						->setTo($mailInterface->recipientEmailList);

					$this->logger->info('$mailInterface->recipientEmailList', [$mailInterface->recipientEmailList, __METHOD__, __LINE__]);

				} catch (Exception $e) {
					$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
				}

				try {
					$countRecipients = $transport->send($swiftMessage);
					$this->logger->info("RECIPIENTS: " . json_encode(['$countRecipients' => $countRecipients]), [__METHOD__, __LINE__]);
				} catch (Exception $e) {
					$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
					$returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
				}
				$returnMessage .= ($countRecipients > 0) ? "send email to {$countRecipients} recipient(s)" : 'email sending failed: ' . $countRecipients;
				return (0 == $countRecipients) ? false : true;
			}
		} else {
			$returnMessage .= 'email not send: no content provided. e' . __LINE__;
			return false;
		}
	}


	/**
	 * returns array <b>[ccRecipientEmailList,  messageBody,  messageSubject,  recipientEmailList,  recipientIsBcc,  senderEmailAddress,  senderFullname,  senderPassword,  smtpPort,  smtpServer]</b>
	 * <br>with basic configuration from <b>App\Types\MsMailInterfaceTypes</b>
	 * @return array
	 */
	public function getBaseConfiguration():array
	{
		return $this->mailInterfaceTypes->getInitialConfigArray();
	}


}
