<?php

namespace App\Services;

class SecurityService
{
    /**
     * @internal must be call by each and every route before proceeding
     * @param string $ip
     * @return bool
     */
    public function isAllowedIp(string $ip): bool
    {
        // defines all ipv4 / ipv6, which are allowed to call routes of this Microservice
        return in_array($ip, [
            '127.0.0.1', // localhost
            '185.158.183.223', // office ip
            '2001:1520:1:200::349', // MIO TEST
            '62.138.185.206', // MIO TEST
            '172.31.13.27', // aws-api-in-one production private ip
            '172.31.36.31', // aws-marketing-in-one production private ip
            '18.196.32.177', // aws-api-in-one production public ip
            '18.196.60.233', // aws-marketing-in-one production public ip
        ]);
    }
}
