<?php


namespace App\Controller;

use App\Services\MailService;
use App\Services\MessageService;
use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use App\Types\MsMailInterfaceTypes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Services\ToolService;
use Psr\Log\LoggerInterface;
use Swift_Message;
use Swift_SmtpTransport;


/**
 * Class MailController
 * @package App\Controller
 * @Route("/email")
 */
class MailController extends AbstractController
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/aws/test", name="email_aws_test", methods={"GET"})
     * @param Request     $request
     * @param ToolService $toolService
     */
    public function awsTest(Request $request, LoggerInterface $logger, MsMailInterfaceTypes $mailInterface,MessageParameterTypes $messageParameters, MailService $mailService, EmailContentTypes  $emailContentTypes):JsonResponse
    {
        $returnMessage = '';
        $mailInterface->smtpServer = 'email-smtp.eu-central-1.amazonaws.com';
        $mailInterface->smtpPort = '587';
        $mailInterface->senderEmailAddress = 'support@aws-marketing-in-one.net';
        $mailInterface->senderPassword = 'BMPlZk35dvfQIDdPAlY22cEQTrVN4w1stWRnrbThbKdR';
        $mailInterface->recipientEmailList = ['jochen.schaefer@treaction.net', 'aravind.karri@treaction.net'];
        $mailInterface->messageSubject = "AWS E-Mail Server Configuration";
        $mailInterface->senderFullname = " AWS SES Marketing-In-One";

        $messageParameters->firstName = 'Marketing-In-One';
        $messageParameters->lastName = 'Administrator';

        // $success = $mailService->sendSwiftMail($emailContentTypes->accountTestMailserverNotification, $messageParameters, $mailInterface, $returnMessage);


       $reqHost = $request->getHttpHost();
        $reqIp = $request->getClientIp();
       $mailInterface->messageBody = "<h1>TEST AWS SES</h1><p>it seems to work ... </p><p>send from $reqHost / $reqIp</p><p>using endpoint /email/aws/test</p>";

        if (!empty($mailInterface->recipientEmailList)) {

            if (empty($mailInterface->messageBody)) {
                $returnMessage .= 'no message body e' . __LINE__;
            } else {
                $countRecipients = 0;

                $this->logger->info('$mailContent=' . print_r($mailInterface->messageBody, true), [__METHOD__, __LINE__]);

                try {
                    if(587 === (int)$mailInterface->smtpPort) {
                        $transport = new Swift_SmtpTransport($mailInterface->smtpServer, (int)$mailInterface->smtpPort, 'tls');

                    } else {
                        $transport = new Swift_SmtpTransport($mailInterface->smtpServer, (int)$mailInterface->smtpPort );

                    }
                    // AWS SES USER / PW
                    $transport->setUsername('AKIATFUNGCAY5USXIDNM')->setPassword('BG1AHdgycKa0mhuUzCzeE1WCU0vvvfXiqP5Oxfa9gOAU');
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    $returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
                }

                try {
                    /**
                     * uses html with prio
                     */
                    $swiftMessage = new Swift_Message();
                    $this->logger->info('setFrom', ['senderEmailAddress' => $mailInterface->senderEmailAddress, 'senderFullname' => $mailInterface->senderFullname, __METHOD__, __LINE__]);
                    $swiftMessage->setContentType('text/html')
                        ->setSubject($mailInterface->messageSubject)
                        ->setBody($mailInterface->messageBody,'text/html')
                        ->setFrom([$mailInterface->senderEmailAddress => $mailInterface->senderFullname])
                        ->setTo($mailInterface->recipientEmailList);

                    $this->logger->info('$mailInterface->recipientEmailList', [$mailInterface->recipientEmailList, __METHOD__, __LINE__]);

                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    $returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
                }

                try {
                    $countRecipients = $transport->send($swiftMessage);
                    $this->logger->info("RECIPIENTS: " . json_encode(['$countRecipients' => $countRecipients]), [__METHOD__, __LINE__]);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    $returnMessage .= $e->getMessage() . ' e' . __LINE__ . ', ';
                }
                $returnMessage .= ($countRecipients > 0) ? "send email to {$countRecipients} recipient(s)" : 'email sending failed: ' . $countRecipients;

            }
        } else {
            $returnMessage .= 'email not send: no content provided. e' . __LINE__;
        }

        return $this->json(['countRecipients' => $countRecipients, 'message' => $returnMessage]);

    }


    /**
     * @Route("/test_cio_config", name="send_email_test_cio_config", methods={"POST"})
     * @param Request     $request
     * @param ToolService $toolService
     */
    public function testConfigByCio(
    	Request $request,
		ToolService $toolService,
		MsMailInterfaceTypes $msMailInterfaceTypes,
		MailService $mailService,
		EmailContentTypes  $emailContentTypes,
		MessageParameterTypes $messageParameterTypes,
		LoggerInterface $logger)
    {
        $logger->info('PACKED CONTENT  =' . $request->getContent(), [__METHOD__, __LINE__]);
        $unpacked_content = json_decode(base64_decode($request->getContent()));
        $logger->info('UNPACKED CONTENT=' . print_r($unpacked_content, true), [__METHOD__, __LINE__]);


		$content = $toolService->unpackContent($request->getContent() );
		$msMailInterfaceTypes->smtpServer = $content['smtpServer'];
		$msMailInterfaceTypes->smtpPort = $content['smtpPort'];
		$msMailInterfaceTypes->senderEmailAddress = $content['senderEmailAddress'];
		$msMailInterfaceTypes->senderUsername = $content['senderEmailAddress'];
		$msMailInterfaceTypes->senderPassword = $content['senderPassword'];
		$msMailInterfaceTypes->recipientEmailList = [$content['recipientEmailList']];
		$msMailInterfaceTypes->messageSubject = "Successful E-Mail Server Configuration";
		$msMailInterfaceTypes->senderFullname = "Marketing-In-One";
		$messageParameterTypes->firstName = (!empty($content['firstname'])) ? $content['firstname'] :'Marketing-In-One';
		$messageParameterTypes->lastName = (!empty($content['lastname'])) ? $content['lastname'] : 'Administrator';

        $logger->info('$msMailInterfaceTypes configuration',[$msMailInterfaceTypes->getConfiguration(), __METHOD__, __LINE__]);

		$success = $mailService->sendSwiftMail($emailContentTypes->accountTestMailserverNotification, $messageParameterTypes, $msMailInterfaceTypes, $returnMessage);

		if($success) {
			return $this->json(['status' => true, 'message' => $returnMessage]);
		} else {
			return $this->json(['status' => false, 'message' => $returnMessage]);
		}
    }


    /**
     * @Route("/send/simplemail", name="sendSimpleMail", methods={"POST"})
     * @param Request     $request
     * @param ToolService $toolService
     */
    public function sendSimpleMail(
        Request $request,
        ToolService $toolService,
        MsMailInterfaceTypes $msMailInterfaceTypes,
        MailService $mailService,
        EmailContentTypes  $emailContentTypes,
        MessageParameterTypes $messageParameterTypes,
        LoggerInterface $logger)
    {
        $emailPattern = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7Eäöüß]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7Fäöüß]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9äöüß]+(?:-[a-z0-9äöüß]+)*\.){1,126}){1,}(?:(?:[a-zäöüß][a-z0-9äöüß]*)|(?:(?:xn--)[a-z0-9äöüß]+))(?:-[a-z0-9äöüß]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iuD';
        $logger->info('PACKED CONTENT  =' . $request->getContent(), [__METHOD__, __LINE__]);
        $unpacked_content = json_decode(base64_decode($request->getContent()));
        $logger->info('UNPACKED CONTENT=' . print_r($unpacked_content, true), [__METHOD__, __LINE__]);

        $content = $toolService->unpackContent($request->getContent() );
        if( !isset($content['routeAccessKey']) || $content['routeAccessKey'] !== "uh{ajaegu%e&GhoPoh4no4ree2ue0akaiyach7lai|y;ie5ooghoong4koh8aiTh" ) {
            return $this->json(['status' => false, 'message' => '401 - Unauthorized']);
        }

        $recipientEmailList = explode(',', $content['recipientEmailList']);
        $this->logger->info('$recipientEmailList',[$recipientEmailList, __METHOD__, __LINE__]);

        foreach ($recipientEmailList as $checkEmail) {
            if(0 === preg_match($emailPattern, $checkEmail)) { // email is invalid
                return $this->json(['status' => false, 'message' => 'invalid email address detected']);
            }
        }

        $messageSubject = substr($content['messageSubject'], 0, 80); // reduce subject to 80 chars only to prevent large content
        $messageBody = preg_replace(['/<iframe>.*<\/iframe>/', '/<script>.*<\/script>/', '/http/', '/img/'], ['', '', 'h_t_t_p_', 'i_m_g'], $content['messageBody']); // remove iframe, scripts, links and images from html for some more secure content

        $msMailInterfaceTypes->smtpServer = $content['smtpServer'];
        $msMailInterfaceTypes->smtpPort = $content['smtpPort'];
        $msMailInterfaceTypes->senderEmailAddress = $content['senderEmailAddress'];
        $msMailInterfaceTypes->senderPassword = $content['senderPassword'];
        $msMailInterfaceTypes->recipientEmailList = $recipientEmailList;
        $msMailInterfaceTypes->messageSubject = $messageSubject;
        $msMailInterfaceTypes->messageBody = $messageBody;
        $msMailInterfaceTypes->senderFullname = $content['senderFullName'];
        $messageParameterTypes->firstName = (!empty($content['firstname'])) ? $content['firstname'] :'';
        $messageParameterTypes->lastName = (!empty($content['lastname'])) ? $content['lastname'] : '';

        // CAUTION: the sympfony/swiftmailer version this installation does not support non-ASCII characters in email addresses (aka RFC 6532 support)
        //          this insatllation must be moved to "composer require symfony/mailer" and code must be adapted !!!
        $success = $mailService->sendSwiftMail($emailContentTypes->blank, $messageParameterTypes, $msMailInterfaceTypes, $returnMessage);

        if($success) {
            return $this->json(['status' => true, 'message' => $returnMessage]);
        } else {
            return $this->json(['status' => false, 'message' => $returnMessage]);
        }
    }


    /**
     * @Route("/send", name="emailSend", methods={"POST"})
     * @param Request $request
     */
    public function sendEmailByCioSmtp(Request $request, ToolService $toolService)
    {
        $returnMessage = '';
        $content = $request->getContent();

        $this->logger->error('ARRIVED CONTENT: ',['$content' => $content, __METHOD__,__LINE__]);

        if(empty($content)){
            return $this->json(['success' => false, 'message' => 'no data provided']);
        }

        if ($toolService->sendMailBySmtp($content, $returnMessage)) {
            return $this->json(['success' => true, 'message' => $returnMessage]);
        } else {
            return $this->json(['success' => false, 'message' => $returnMessage]);
        }
    }


    /**
     * @Route("/cio_smpt_settings", name="cio_smpt_settings", methods={"GET"})
     */
    public function getCioStmpSettings(ToolService $toolService, LoggerInterface $logger)
    {
        $smpt = $toolService->getSmtpSettings();
        // $logger->info(json_encode($smpt->toArray()), [__METHOD__, __LINE__]);
        return $this->json(($smpt->toArray()));
    }


    /**
     * @Route("/params", name="params", methods={"GET"}))
     * @param ToolService $toolService
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getEmailSettings(ToolService $toolService)
    {
        return $this->json($toolService->getEmailSettings());
    }


	/**
	 * @Route("/sendtestmail", name="email.sendtestmail", methods={"POST"}))
	 * @param ToolService $toolService
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function sendTestMail(Request $request, MailService $mailService, MsMailInterfaceTypes $mailInterfaceTypes):JsonResponse
	{
		$email = $request->getContent();
		$returnMessage = '';

		$param = $mailInterfaceTypes;
		$customConfig = $mailInterfaceTypes->getInitialConfigArray();

		$customConfig[$param->messageBody] = "Hallo Du,<br>das ist ein Test";
		$customConfig[$param->messageSubject] = "Test E-Mail von MSMAIL";
		$customConfig[$param->senderFullname] = "MSMAIL";
		$customConfig[$param->recipientEmailList] = [$email];
		// $customConfig[$param->senderEmailAddress] = 'support@dev-campaign-in-one.net';

		$config = array_merge($param->getInitialConfigArray(), $customConfig);

		$config = $mailInterfaceTypes->setConfiguration($config);
		$res = $mailService->sendSwiftMail($config, $returnMessage);

		return $this->json(['msg' => $returnMessage]);
	}


	/**
	 * @deprecated
	 * @Route("/sendswiftmail", name="email.sendswiftmail", methods={"POST"}))
	 * @param ToolService $toolService
	 * @return \Symfony\Component\HttpFoundation\JsonResponse
	 */
	public function sendSwiftMailDeprecated(Request $request, MailService $mailService, MsMailInterfaceTypes $mailInterfaceTypes, EmailContentTypes $emailContentTypes):JsonResponse
	{
		$payload = json_decode(base64_decode($request->getContent()), true, 100, JSON_OBJECT_AS_ARRAY);
		$customConfig = $payload['mailInterface'];


		// early exit
		if(empty($payload['emailContentTypes']) || !in_array( $payload['emailContentTypes'], $emailContentTypes->getEmailContentTypesArray())) {
			$tempName = (empty($payload['emailContentTypes'])) ? 'null' : $payload['emailContentTypes'];
			return $this->json(['msg' => "undefined or wrong email_content_type ({$tempName})"]);
		}
		$returnMessage = '';
		$customConfig = $mailInterfaceTypes->getInitialConfigArray();

		$customConfig[$mailInterfaceTypes->messageBody] = "Hallo Du,<br>das ist ein Test";
		$customConfig[$mailInterfaceTypes->messageSubject] = "Test E-Mail von MSMAIL";
		$customConfig[$mailInterfaceTypes->senderFullname] = "MSMAIL";

		// $customConfig[$param->senderEmailAddress] = 'support@dev-campaign-in-one.net';
		$config = array_merge($mailInterfaceTypes->getInitialConfigArray(), $customConfig);

		$config = $mailInterfaceTypes->setConfiguration($config);
		$res = $mailService->sendSwiftMail($config, $returnMessage);

		return $this->json(['msg' => $returnMessage]);
	}



	/**
	 * @Route("/configuration/params", name="email.configurationparams", methods={"GET"}))
	 * @param MsMailInterfaceTypes $mailInterfaceTypes
	 * @return JsonResponse
	 */
	public function getConfigurationParams( MsMailInterfaceTypes $mailInterfaceTypes, EmailContentTypes $emailContentTypes, MessageParameterTypes $messageParameterTypes) : JsonResponse
	{
		return $this->json([
			'mailInterface' => $mailInterfaceTypes->getInitialConfigArray(),
			'emailContentTypes' => $emailContentTypes->getEmailContentTypesArray(),
			'messageParameterTypes' => $messageParameterTypes->getMessageParameterTypesArray(),
		]);
	}


/*
accountDeletionNotification
accountRegistrationNotification
accountSendActivationReminder
accountSendActivationlink
accountTestMailserverNotification
accountVerifyApikey
aioAccountSendActivationlink
aioSendLeadNotification
*/

	/**
	 * @Route("/sendstandardmail", name="email.sendstandardmail", methods={"POST"}))
	 * @param Request               $request
	 * @param MailService           $mailService
	 * @param EmailContentTypes     $emailContentTypes
	 * @param MessageParameterTypes $messageParameterTypes
	 * @param MsMailInterfaceTypes  $mailInterfaceTypes
	 * @return JsonResponse
	 */
	public function sendStandardMail(Request $request, MailService $mailService, EmailContentTypes $emailContentTypes, MessageParameterTypes $messageParameterTypes, MsMailInterfaceTypes $mailInterfaceTypes):JsonResponse
	{
		$returnMsg = '';
        $postContent = $request->getContent();
		$data = json_decode(base64_decode($postContent), true, 512, JSON_OBJECT_AS_ARRAY);
		/*
		$data['emailContentTypes']; // one string of EmailContentTypes->{key} e.g. "account-deletion-notification", where key is "accountDeletionNotification"
		$data['messageParameterTypes']; // firstName, lastName, email, activationLink, ..., filled with values (then processed) or empty '' (not processed)
		$data['mailInterface']; // configuration settings of email => senderName, senderPw, smtpserver, port, and so on
		*/
        $this->logger->info('postContent', [$postContent, __FUNCTION__, __LINE__]);
		$this->logger->info('$data', [$data, __FUNCTION__, __LINE__]);
		$this->logger->info('$data[emailContentTypes]', [$data['emailContentTypes'], __FUNCTION__, __LINE__]);
		$this->logger->info('$data[messageParameterTypes]', [$data['messageParameterTypes'], __FUNCTION__, __LINE__]);
		$this->logger->info('$data[mailInterface]', [$data['mailInterface'], __FUNCTION__, __LINE__]);



		// early exit
		$propertyName = $emailContentTypes->getPropertyName($data['emailContentTypes']);

		if(!property_exists(EmailContentTypes::class, $propertyName)) {
			return $this->json(['msg' => "email content type {$data['emailContentTypes']} does not exist"]);
		}

		// get & set message parameter types form request body
		foreach($data['messageParameterTypes'] as $type => $value) {
			$messageParameterTypes->{$type} = $value; // firstName, lastName, email, activationLink, ...
		}

        $this->logger->info('$messageParameterTypes', [$messageParameterTypes, __FUNCTION__, __LINE__]);

		// get & set configuration settings of email => messageSubject, senderName, senderPw, smtpserver, port, and so on (body will be overwritten in $mailService->sendSwiftMail)
		foreach($data['mailInterface'] as $type => $value) {
			$mailInterfaceTypes->{$type} = $value;
            $this->logger->info('data[mailInterface]', ['type' => $type, 'value' => $value, __FUNCTION__, __LINE__]);
		}

		$this->logger->info('$mailInterfaceTypes', [$mailInterfaceTypes, __FUNCTION__, __LINE__]);

		try {
			$success = $mailService->sendSwiftMail(
				$emailContentTypes->{$propertyName},
				$messageParameterTypes,
				$mailInterfaceTypes,
				$returnMsg
			);
            $this->logger->info('success, returnMsg', [$success, $returnMsg, __METHOD__, __LINE__]);
		} catch (\Exception $e) {
			$this->logger->error('Error sending email ', [$e->getMessage(), $e->getCode(), __METHOD__, __LINE__]);
			return $this->json(['msg' => 'error: '.$e->getMessage() .', code:'. $e->getCode() . 'failed to send email. e'.__LINE__]);
		}

		if($success) {
			return $this->json(['msg' => $returnMsg]);
		} else {
			return $this->json(['msg' => "Error: {$returnMsg}. Failed to send email. e".__LINE__]);
		}
	}


	/**
	 * @Route("/check/alive", methods={"GET"})
	 */
	public function checkAlive(Request $request)
	{
		return $this->json([
			'alive' => true,
			'your IP' => $request->getClientIp(),
			'toUrl' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]",
		]);
	}

}
