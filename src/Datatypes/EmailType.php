<?php


namespace App\Datatypes;

use App\Services\ToolService;
use Psr\Log\LoggerInterface;


class EmailType
{
    const SENDEREMAILADDRESS = 'senderEmailAddress';
    const SENDERPASSWORD = 'senderPassword';
    const SMTPSERVER = 'smtpServer';
    const SMTPPORT = 'smtpPort';
    const STRINGSENDERFULLNAME = 'senderFullname';
    const BOOLRECIPIENTISBCC = 'recipientIsBcc';
    const STRINGRECIPIENTEMAILLIST = 'recipientEmailList';
    const STRINGCCRECIPIENTEMAILLIST = 'ccRecipientEmailList';
    const STRINGSUBJECT = 'messageSubject';
    const STRINGMESSAGEBODY = 'messageBody';

    protected $boolRecipientIsBcc;  // default false
    protected $ccEmailList;
    protected $recipientEmailList;
    protected $senderPw;
    protected $smtpPort;
    protected $smtpServer;
    protected $stringCcRecipientEmailList;
    protected $stringMessageBody;
    protected $stringMessageBodyHtml;
    protected $stringRecipientEmailList;
    protected $stringSenderEmailAddress;
    protected $stringSenderFullname;
    protected $stringSubject;

    protected $logger;


    /**
     * EmailType constructor.
     * @param array       $mailContent
     * @param ToolService $toolService
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    public function init(array $mailContent)
    {
        $this->logger->info('IN INIT', [__METHOD__, __LINE__]);
        // mandatory
        $this->stringSenderEmailAddress = $mailContent[self::SENDEREMAILADDRESS] ?? '';
        $this->senderPw = $mailContent[self::SENDERPASSWORD] ?? '';
        $this->smtpServer = $mailContent[self::SMTPSERVER] ?? '';
        $this->smtpPort = $mailContent[self::SMTPPORT] ?? '';

        $this->stringRecipientEmailList = $mailContent[self::STRINGRECIPIENTEMAILLIST] ?? '';
        $this->recipientEmailList = $this->getEmailList($this->stringRecipientEmailList);

        if ($this->logger) {
            $this->logger->info(__CLASS__ . ' EMAIL-SUBJECT = ' . $mailContent[self::STRINGSUBJECT], [__METHOD__, __LINE__]);
            $this->logger->info(__CLASS__ . ' EMAIL-SUBJECT SANITIZED = ' . filter_var(htmlentities((string)$mailContent[self::STRINGSUBJECT]),
                    FILTER_SANITIZE_STRING), [__METHOD__, __LINE__]);
        }
        //TODO: remove if, after api-in-one is fixed
        if (is_string($mailContent[self::STRINGSUBJECT])) {
            $stringSubject = filter_var(htmlentities((string)$mailContent[self::STRINGSUBJECT]), FILTER_SANITIZE_STRING);
            $this->stringSubject = (!$stringSubject) ? 'your campaign generated a new lead' : $stringSubject;
        } else {
            $this->stringSubject = 'your campaign generated a new lead';
        }
        // $this->stringMessageBody = filter_var(htmlentities((string) $mailContent[self::STRINGMESSAGEBODY]), FILTER_SANITIZE_STRING) ?? '';

        $this->logger->error('$mailContent[self::STRINGMESSAGEBODY] = ' . print_r($mailContent[self::STRINGMESSAGEBODY], true), [__METHOD__, __LINE__]);
        $this->setStringMessageBody($mailContent[self::STRINGMESSAGEBODY]);
        $this->stringMessageBodyHtml = $mailContent[self::STRINGMESSAGEBODY];
        // $this->logger->info('$this->stringMessageBodyHtml = ' . $this->stringMessageBodyHtml, [__METHOD__, __LINE__]);
        // $this->logger->error('$this->stringMessageBody = ' . $this->stringMessageBody, [__METHOD__, __LINE__]);

        // optional
        $this->stringSenderFullname = $mailContent[self::STRINGSENDERFULLNAME] ?? '';
        $this->boolRecipientIsBcc = $mailContent[self::BOOLRECIPIENTISBCC] ?? false;
        $this->stringCcRecipientEmailList = $mailContent[self::STRINGCCRECIPIENTEMAILLIST] ?? '';
        $this->ccEmailList = $this->getEmailList($this->stringCcRecipientEmailList);
    }


    protected function getEmailList(string $recipientEmailList): array
    {
        return explode(',', $recipientEmailList);
//        $listtemp = explode(',', $recipientEmailList);
//        $list = [];
//        foreach ($listtemp as $email) {
//            $sanEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
//            $validEmail = filter_var($sanEmail, FILTER_VALIDATE_EMAIL);
//
//            if($validEmail === $email) {
//                $list[$email] = '';
//            }
//        }
//        return $list;
    }


    /**
     * @param array $messageBody
     * @return string
     */
    protected function getMessageBodyAsString(array $messageBody): string
    {
        $str = '';
        foreach ($messageBody as $k => $v) {
            $str .= "$k : $v\n";
        }
        return $str;
    }





    /**
     * @return array
     */
    public static function getMailingKeyList(): array
    {
        return [
            self::SENDEREMAILADDRESS => "string, optional",
            self::SENDERPASSWORD => "string, optional, encrypted",
            self::SMTPSERVER => "string, optional",
            self::SMTPPORT => "string, optional",
            self::STRINGSENDERFULLNAME => "string, optional",
            self::BOOLRECIPIENTISBCC => "bool, optional",
            self::STRINGRECIPIENTEMAILLIST => "string, mandatory",
            self::STRINGCCRECIPIENTEMAILLIST => "string, optional",
            self::STRINGSUBJECT => "string, mandatory",
            self::STRINGMESSAGEBODY => "string, mandatory",
        ];
    }


    /**
     * @return string
     */
    public function getSenderPw(): string
    {
        return $this->senderPw;
    }


    /**
     * @param string $senderPw
     */
    public function setSenderPw(string $senderPw): void
    {
        $this->senderPw = $senderPw;
    }


    /**
     * @return string
     */
    public function getStringMessageBody(): string
    {
        return $this->stringMessageBody;
    }


	/**
	 * @return string
	 */
	public function getStringMessageBodyHtml():string
	{
		return $this->stringMessageBodyHtml;
	}



    /**
     * @return array
     */
    public function getRecipientEmailList(): array
    {
        return $this->recipientEmailList;
    }


    /**
     * @return array
     */
    public function getCcEmailList(): array
    {
        return $this->ccEmailList;
    }


    /**
     * @return bool
     * @author jsr
     */
    public function isReadyToSend(): bool
    {
        return (!empty($this->smtpServer) && !empty($this->smtpPort) && !empty($this->stringSenderEmailAddress) && !empty($this->senderPw) && $this->stringSenderEmailAddress === filter_var($this->stringSenderEmailAddress,
                FILTER_VALIDATE_EMAIL) && !empty($this->recipientEmailList) && !empty($this->stringSubject) && !empty($this->stringMessageBody));
    }


    /**
     * @return string
     */
    public function getStringSubject(): string
    {
        return $this->stringSubject;
    }


    /**
     * @return string
     */
    public function getStringSenderEmailAddress(): string
    {
        return $this->stringSenderEmailAddress;
    }


    /**
     * @param string $stringSenderEmailAddress
     */
    public function setStringSenderEmailAddress(string $stringSenderEmailAddress): void
    {
        $this->stringSenderEmailAddress = $stringSenderEmailAddress;
    }


    /**
     * @return mixed|string
     */
    public function getSmtpPort()
    {
        return $this->smtpPort;
    }


    /**
     * @param mixed (int|string) $smtpPort
     */
    public function setSmtpPort($smtpPort): void
    {
        $this->smtpPort = $smtpPort;
    }


    /**
     * @return string
     */
    public function getSmtpServer(): string
    {
        return $this->smtpServer;
    }


    /**
     * @param mixed|string $stringSenderFullname
     */
    public function setStringSenderFullname($stringSenderFullname): void
    {
        $this->stringSenderFullname = $stringSenderFullname;
    }


    /**
     * @return mixed|string
     */
    public function getStringSenderFullname()
    {
        return $this->stringSenderFullname;
    }


    /**
     * @param mixed|string $stringSubject
     */
    public function setStringSubject($stringSubject): void
    {
        $this->stringSubject = $stringSubject;
    }


    /**
     * @param string|array $messageBody
     */
    public function setStringMessageBody($messageBody): void
    {
        if (is_array($messageBody)) {
            $this->stringMessageBody = $this->getMessageBodyAsString($messageBody);
        } elseif (is_string($messageBody)) {
            $this->stringMessageBody = $messageBody;
        } else {
            $this->stringMessageBody = print_r($messageBody, true);
        }
    }


    /**
     * @param string $smtpServer
     */
    public function setSmtpServer(string $smtpServer): void
    {
        $this->smtpServer = $smtpServer;
    }


    /**
     * @return EmailType
     * @author jsr
     */
    public function get(): EmailType
    {
        return $this;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string)json_encode($this);
    }

}


