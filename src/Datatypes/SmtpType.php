<?php
/**
 * @author jsr
 */
namespace App\Datatypes;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

class SmtpType implements LoggerAwareInterface
{
    protected $sender;
    protected $senderPw;
    protected $smtpServer;
    protected $smtpPort;

    protected $logger;

    public function __construct()
    {
        $this->smtpServer = '';
        $this->sender = '';
        $this->senderPw = '';
        $this->smtpPort = '';
    }


    /**
     * @return SmtpType
     */
    public function get(): SmtpType
    {
        // $this->logger->info('IN GET = ' . json_encode($this->toArray()), [__METHOD__, __LINE__]);
        return $this;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['sender' => $this->sender, 'senderPw'=> $this->senderPw, 'smtpServer' => $this->smtpServer, 'smtpPort' => $this->smtpPort];
    }


    /**
     * @return string
     */
    public function getSenderPw(): string
    {
        return $this->senderPw;
    }


    /**
     * @param string $senderPw
     */
    public function setSenderPw(string $senderPw): void
    {
        $this->senderPw = $senderPw;
    }


    /**
     * @param string $sender
     */
    public function setSender(string $sender): void
    {
        $this->sender = $sender;
    }


    /**
     * @param string $smtpPort
     */
    public function setSmtpPort(string $smtpPort): void
    {
        $this->smtpPort = $smtpPort;
    }


    /**
     * @param string $smtpServer
     */
    public function setSmtpServer(string $smtpServer): void
    {
        $this->smtpServer = $smtpServer;
    }


    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }


    /**
     * @return string
     */
    public function getSmtpPort(): string
    {
        return $this->smtpPort;
    }


    /**
     * @return string
     */
    public function getSmtpServer(): string
    {
        return $this->smtpServer;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        $ret = ['sender' => $this->sender, 'senderPw'=>$this->senderPw, 'smtpServer' => $this->smtpServer, 'smtpPort' => $this->smtpPort];
        return (string)json_encode($ret);
    }


    /**
     * @inheritDoc
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
