<?php


namespace App\Types;


class PlaceholderTypes
{
	public string $ACCOUNTNO = "###ACCOUNTNO###";
	public string $ACTIVATIONLINK = "###ACTIVATIONLINK###";
	public string $APIKEY = "###APIKEY###";
	public string $EMAIL = "###EMAIL###";
	public string $FIRSTNAME = "###FIRSTNAME###";
	public string $IMGURL = "###IMGURL###";
	public string $LASTNAME = "###LASTNAME###";
	public string $CUSTOMCONTENT = "###CUSTOMCONTENT###";
	public string $SERVERURL = "###SERVERURL###";
	public string $MIOURL = "###MIOURL###";


	public function __construct()
	{
	}


	public function getPlaceholderTypesArray()
	{
		return [
			$this->IMGURL,
			$this->FIRSTNAME,
			$this->LASTNAME,
			$this->EMAIL,
			$this->ACCOUNTNO,
			$this->ACTIVATIONLINK,
			$this->APIKEY,
			$this->CUSTOMCONTENT,
			$this->SERVERURL,
			$this->MIOURL,
		];
	}

}
