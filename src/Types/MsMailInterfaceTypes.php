<?php
/* THIS SHOULD BE REENGINEERED */
namespace App\Types;
use App\Services\SecureService;
use Psr\Log\LoggerInterface;

/**
 * MIO, AIO, MSMAIL
 * Class MsMailInterfaceTypes
 * @package App\Types
 */
class MsMailInterfaceTypes
{
	public $ccRecipientEmailList = '';
	public $messageBody = '';
	public $messageSubject = '';
	public $recipientEmailList = '';
	public $recipientIsBcc = false;
	public $senderEmailAddress = '';
	public $senderFullname = 'Marketing-In-One';
    public $senderUsername = '';
	public $senderPassword = '';
	public $smtpPort = 587;
	public $smtpServer = '';

	private bool $configured;
	private $decryptedPw;
	private LoggerInterface $logger;

	public function __construct(
        SecureService $secureService,
        LoggerInterface $logger,
        string $defaultSenderEmailAddress, // from services_*.yaml
        string $defaultSenderUsername, // from services_*.yaml
        string $defaultSenderPassword,  // from services_*.yaml
        string $defaultSmtpPort,  // from services_*.yaml
        string $defaultSmtpServer  // from services_*.yaml
    )
	{
        $this->logger = $logger;
        $logger->info('$defaultSenderEmailAddress',[$defaultSenderEmailAddress, __METHOD__, __LINE__]);
        $logger->info('$defaultSenderUsername',[$defaultSenderUsername, __METHOD__, __LINE__]);
        $logger->info('$defaultSenderPassword',[$defaultSenderPassword, __METHOD__, __LINE__]);
        $logger->info('$defaultSmtpServer',[$defaultSmtpServer, __METHOD__, __LINE__]);
        $logger->info('$defaultSmtpPort',[$defaultSmtpPort, __METHOD__, __LINE__]);
        $this->initDefaultSettings(
            $defaultSenderEmailAddress,
            $defaultSenderUsername,
            $secureService->getDecryptedPasswordFromSecureInOne($defaultSenderPassword),
            $defaultSmtpPort,
            $defaultSmtpServer
        );
        $logger->info('$senderPassword',[$this->senderPassword, __METHOD__, __LINE__]);
        $this->decryptedPw = $this->senderPassword;
		$this->configured = true;

//		$this->init();

//		$logger->info('SMTPSRV_CD', [$_ENV['SMTPSRV_CD'], __METHOD__, __LINE__]);
//        if( isset($_ENV['SMTPSRV_CD'])) {
//            $this->decryptedPw = $secureService->getDecryptedPasswordFromSecureInOne($_ENV['SMTPSRV_CD']);
//        } else {
//            $this->decryptedPw = '';
//        }

		$logger->info('PW (length) / (1st 4 char)', [strlen($this->decryptedPw), substr($this->decryptedPw, 0, 4).'...', __METHOD__, __LINE__]);
	}


    /**
     * init
     * @param string $defaultSenderEmailAddress
     * @param string $defaultSenderUsername
     * @param string $defaultSenderPassword
     * @param string $defaultSmtpPort
     * @param string $defaultSmtpServer
     * @return void
     */
    private function initDefaultSettings(
        string $senderEmailAddress,
        string $senderUsername,
        string $senderPassword,
        string $smtpPort,
        string $smtpServer
    ) {
        $this->senderEmailAddress = $senderEmailAddress;
        $this->senderUsername = $senderUsername;
        $this->senderPassword = $senderPassword;
        $this->smtpPort = $smtpPort;
        $this->smtpServer = $smtpServer;

        if( $_ENV['APP_ENV'] === 'aws') {
            try {
                file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/mailsettings.txt', json_encode(
                    [
                        'senderEmailAddress' => $this->senderEmailAddress,
                        'senderUsername' => $this->senderUsername,
                        'senderPassword' => $this->senderPassword,
                        'smtpPort' => $this->smtpPort,
                        'smtpServer' => $this->smtpServer,
                        'm' =>__METHOD__,
                        'l' => __LINE__,
                    ]
                ));
            } catch (\Exception $e) {
                ; // do nothing
            }
        }
    }


	/**
	 * @return array [ccRecipientEmailList,messageBody,messageSubject,recipientEmailList,recipientIsBcc,senderEmailAddress,senderFullname,senderPassword,smtpPort,smtpServer]<br>with basic configuration
	 */
//	public function getInitialConfigArray(): array
//	{
//		return [
//			$this->ccRecipientEmailList => '',
//			$this->messageBody => '',
//			$this->messageSubject => 'E-Mail from Marketing-In-One',
//			$this->recipientEmailList => '',
//			$this->recipientIsBcc => false,
//			$this->senderEmailAddress => isset($_ENV['SMTPSRV_SENDER']) ? $_ENV['SMTPSRV_SENDER']: '',
//			$this->senderFullname => 'Marketing-In-One',
//			$this->senderPassword => $this->decryptedPw,
//			$this->smtpPort => isset($_ENV['SMTPSRV_PORT']) ? $_ENV['SMTPSRV_PORT'] : '',
//			$this->smtpServer => isset($_ENV['SMTPSRV_URL']) ? $_ENV['SMTPSRV_URL'] :'',
//		];
//	}

	/**
     * used by AIO
	 * @return array [ccRecipientEmailList,messageBody,messageSubject,recipientEmailList,recipientIsBcc,senderEmailAddress,senderFullname,senderPassword,smtpPort,smtpServer]<br>with basic configuration
	 */
	public function getInitialConfigArray(): array
	{
		return [
            'ccRecipientEmailList' => $this->ccRecipientEmailList,
            'messageBody' => $this->messageBody,
            'messageSubject' => $this->messageSubject,
            'recipientEmailList' => $this->recipientEmailList,
            'recipientIsBcc' => $this->recipientIsBcc,
            'senderEmailAddress' => $this->senderEmailAddress,
            'senderFullname' => $this->senderFullname,
            'senderUsername' => $this->senderUsername,
            'senderPassword' => $this->senderPassword,
            'smtpPort' => $this->smtpPort,
            'smtpServer' => $this->smtpServer,
		];
	}


	/**
	 * @return MsMailInterfaceTypes with  basic configuration
     * @deprecated
	 */
	public function getInitialConfigObject(): MsMailInterfaceTypes
	{
		$obj = clone $this;
		$obj->ccRecipientEmailList = '';
		$obj->messageBody = '';
		$obj->messageSubject = 'E-Mail from Marketing-In-One';
		$obj->recipientEmailList = '';
		$obj->recipientIsBcc = false;
		$obj->senderEmailAddress = isset($_ENV['SMTPSRV_SENDER']) ? $_ENV['SMTPSRV_SENDER']: '';
		$obj->senderFullname = 'Marketing-In-One';
		$obj->senderPassword =  $this->decryptedPw;
		$obj->smtpPort = isset($_ENV['SMTPSRV_PORT']) ? $_ENV['SMTPSRV_PORT'] : '';
		$obj->smtpServer = isset($_ENV['SMTPSRV_URL']) ? $_ENV['SMTPSRV_URL'] :'';
		return $obj;
	}


	/**
	 * @param array $myconfig
	 * @return $this
	 */
	public function setConfiguration(array $myconfig): MsMailInterfaceTypes
	{
		foreach ($myconfig as $k => $v) {
			$this->{$k} = $v;
		}

		$this->configured = true;
		return $this;
	}


	/**
	 * @return $this|null
	 */
	public function getConfiguration(): ?MsMailInterfaceTypes
	{
		if ($this->configured) {
			return $this;
		} else {
			return null;
		}
	}


	/**
	 * @param array|null $mailParameters default null => not used here in MSMAIL, but in MIO, AIO
	 */
	private function init(array $mailParameters=null)
	{
		foreach ($this as $k => $v) {
			$this->{$k} = $k;
		}
		$this->configured= true;
	}

}

