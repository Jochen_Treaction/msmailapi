<?php


namespace App\Types;



/**
 * MIO, AIO, MSMAIL
 * Class EmailContentTypes
 * @package App\Types
 */
class EmailContentTypes
{
	public string $accountFinishSetupNotification = 'account-finish-setup-notification'; // TODO
	public string $accountReactivationNotification = 'account-reactivation-notification'; // TODO
	public string $accountDeletionNotification = 'account-deletion-notification'; // TODO
	public string $accountRegistrationNotification = 'account-registration-notification'; // okay
	public string $accountSendActivationlink = 'account-send-activationlink'; // okay
	public string $accountSendActivationReminder = 'account-send-activation-reminder'; // TODO
	public string $accountSendPasswordResetLink = 'account-send-password-reset-link';
	public string $accountTestMailserverNotification = 'account-test-mailserver-notification'; // TODO
	public string $accountVerifyApikey = 'account-verify-apikey'; // okay
	public string $aioAccountSendActivationlink = 'aio-account-send-activationlink'; // TODO
	public string $aioSendLeadNotification = 'aio-send-lead-notification'; // TODO
    public string $blank = 'blank';


	public function __construct()
	{
	}


	/**
	 * @return array
	 */
	public function getEmailContentTypesArray():array
	{
		$emailContentTypesArray = [];
		foreach ($this as $k => $v) {
			$emailContentTypesArray[$k] = $v;
		}
		return $emailContentTypesArray;
	}


	public function getPropertyName(string $emailContentType): ?string
	{
		foreach ($this as $k => $v) {
			if ($v === $emailContentType) {
				return $k;
			}
		}
		return null;
	}
}
